## Test API

* Python package provided: `api`
* Install dependencies `pip3 install -r requirements.txt`
* To run set env: `export FLASK_APP=api/app.py` then `flask run`
