
from collections import namedtuple
import collections

from collections import namedtuple

__all__ = [
    "ApiError",
    "AuthError",
    "AccessTokenError",
    "error"
]

ApiError = namedtuple("ApiError", ["http_status", "message", "details"])

AuthError = ApiError(401, "CREDENTIALS_INVALID", "Incorrect username or password")
AccessTokenError = ApiError(401, "CREDENTIALS_INVALID", "Incorrect token")
NotFound = ApiError(404, "NOT FOUND", "Page not found")
InvalidEmailError = ApiError(401, "INVALID EMAIL", "The email is invalid")
InvalidPasswordError = ApiError(401, "INVALID PASSWORD", "Password length should be more than 5 characters")

def error(err: ApiError)->tuple:
    return {
        "message": err.message,
        "details": err.details,
    }, err.http_status