import re

from errors import AuthError, AccessTokenError, error, InvalidEmailError, InvalidPasswordError

REGEX_EMAIL = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

def authorize(username:str, password:str)->dict:
    if(not valid_email(username)):
        return error(InvalidEmailError)
    
    if(not valid_password(password)):
        return error(InvalidPasswordError)

    if([username, password] != valid_credentials()):
        return error(AuthError)

    return {
        "message": "SUCCESS",
        "access_token": "cTV0aWFuQ2NqaURGRE82UmZXNVBpdTRxakx3V1F5"
    }, 200

def get_profile(access_token:str)-> dict:
    if(not valid_token(access_token)):
        return error(AccessTokenError)

    return {
        "message": "SUCCESS",
        "platform": "uber",
        "profile": {
            "country": "mx",
            "city_name": "Ciudad de Mexico",
            "worker_id": "34dc0c89b16fd170eba320ab186d08ed",
            "first_name": "Pierre",
            "last_name": "Delarroqua",
            "email": "pierre@palenca.com",
            "phone_prefix": "+52",
            "phone_number": "5576955981",
            "rating": "4.8",
            "lifetime_trips": 1254
        }
    }, 200

def valid_credentials():
    return ["pierre@palenca.com", "MyPwdChingon123"]

def valid_email(email:str)->bool:
    if(re.fullmatch(REGEX_EMAIL, email)):
        return True
    return False

def valid_password(password:str)->bool:
    return len(password) > 5

def valid_token(token:str)->bool:
    return token == 'cTV0aWFuQ2NqaURGRE82UmZXNVBpdTRxakx3V1F5'