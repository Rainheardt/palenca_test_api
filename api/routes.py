from flask import request, Blueprint

from handlers import authorize, get_profile

__all__ = [ "api"]

api = Blueprint("api",  __name__,)

@api.route("/", methods=["GET"])
def print():
    return "<p>Hello Palenka</p>"

@api.route("/uber/login", methods=["POST"])
def login():
    data = request.get_json()
    return authorize(data["username"], data["password"])

@api.route("/uber/profile/<access_token>", methods=["GET"])
def profile(access_token=""):
    return get_profile(access_token)