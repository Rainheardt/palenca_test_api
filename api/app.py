from flask import Flask, request

from routes import api
from errors import NotFound, error
from handlers import authorize

app = Flask(__name__)
app.register_blueprint(api)

@app.errorhandler(404)
def not_found(err):
    return error(NotFound)
